<!DOCTYPE html>
<html class="no-js"
      lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible"
          content="ie=edge">
    <title>Historique détection Lidar</title>
    <meta name="description"
          content="Réupération et visualisation des évènements transmises par le lidar">
    <meta name="viewport"
          content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon"
          href="apple-touch-icon.png">
    <link rel="icon"
          type="image/png"
          href="logo.png"/>

    <link rel="stylesheet"
          href="style.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
<?php

//Connexion à la base de données
try {
    $bdd = new PDO('mysql:host=localhost;dbname=lidar;charset=utf8', 'lidar', 'IiGdt4sEPZS6609y');
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}


//Va prendre en compte une requète avec les variables get 'zone' et 'type'. Ajoute l'heure de réception
if (isset($_GET['zone']) && isset($_GET['type'])) {
    $datetime = date('Y-m-d H:i:s');
    $zoneName = (string)$_GET['zone'];
    $detectionType = (string)$_GET['type'];
    echo 'Intrus détecté dans la zone ' . $zoneName . ', de type ' . $detectionType . ' à l\'heure suivante ' . $datetime;

    $req = $bdd->prepare('INSERT INTO detection_events(detection_time, direction, zone) VALUES
    (:detection_time, :direction, :zone)');
    $req->execute(array(
        'detection_time' => $datetime,
        'direction' => $detectionType,
        'zone' => $zoneName
    ));
}

?>

</body>

</html>
